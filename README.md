# Inventory formula

This formula ensures that all hosts are properly tagged and that the contents of 
the `inventory` pillar are mirrored in the `inventory` grain.
