{% if salt['pillar.get']('inventory', None) %}

updated host inventory:
  grains.present:
    - name: inventory
    - value: {{ salt['pillar.get']('inventory') }}
    - force: True
    - order: last

{% endif %}
